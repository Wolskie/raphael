require 'socket'

s = TCPSocket.open("localhost", 3000)

t = Thread.start {
    puts "Listening .."
    while(buf = s.gets("\x04")) do 
        puts buf
    end
}

s.send(File.read("hello.json") + "\x04", 0)
s.send(File.read("test2.json") + "\x04", 0)

t.join()
