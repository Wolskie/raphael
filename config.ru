require "./raphael.rb"


# Catch everything here, sometimes
# we cant do that in the application for some
# reason.
#
begin
    run RaphaelServer
rescue Exception => e
    abort("Raphael: (config.ru): There is a problem: \n\t#{e.message}")
end

